package TshirtProduct.HibernateProject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import java.util.Collections;

import TshirtProduct.HibernateProject.DAO.DaoProduct;
import TshirtProduct.HibernateProject.TShirt;
import TshirtProduct.HibernateProject.OutputPreference.ComparePrice;
import TshirtProduct.HibernateProject.OutputPreference.CompareRating;

public class TshirtOperation {

	public ArrayList<TShirt> getTshirtMatched(NecessaryTshirtInput necessary) throws IOException {
		ArrayList<TShirt> foundTshirt = new ArrayList<TShirt>();

		DaoProduct tshirtDao = new DaoProduct();

		List<TShirt> tshirtAll = tshirtDao.getAllTShirts();

		for (TShirt tshirt : tshirtAll) {
			if (tshirt.getColor().equalsIgnoreCase(necessary.getColor())
					&& tshirt.getSize().equalsIgnoreCase(necessary.getSize())
					&& tshirt.getGender().equalsIgnoreCase(necessary.getGender())) {
				foundTshirt.add(tshirt);
			}

		}
		Comparator<TShirt> comparatorTshirt = null;
		
		
		if (necessary.getPreference() == 1) {
			comparatorTshirt = new ComparePrice();

		} else if (necessary.getPreference() == 2) {
			comparatorTshirt = new CompareRating();
		} else {
			System.out.println("Wrong Preference choosed!");
		}
		Collections.sort(foundTshirt, comparatorTshirt);

		return foundTshirt;

	}

}
