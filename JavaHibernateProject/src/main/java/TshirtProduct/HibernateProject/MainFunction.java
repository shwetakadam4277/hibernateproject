package TshirtProduct.HibernateProject;

import java.io.IOException;
import java.util.ArrayList;
import TshirtProduct.HibernateProject.TShirt;

import TshirtProduct.HibernateProject.InputDetails;
import TshirtProduct.HibernateProject.OutputDetails;
import TshirtProduct.HibernateProject.TshirtOperation;
import TshirtProduct.HibernateProject.NecessaryTshirtInput;

public class MainFunction {

	public static void main(String[] args) throws IOException{
		
//		taking user inputs from userdetails class
		final InputDetails input=new InputDetails();
		final NecessaryTshirtInput necessary=input.getInputDetails();
		
//		Searching and getting matched tshirts
		final TshirtOperation searchOperation=new TshirtOperation();
		final ArrayList<TShirt> foundTshirt=searchOperation.getTshirtMatched(necessary);
		
//		Class for displaying output
		final OutputDetails output=new OutputDetails();
		output.display(foundTshirt);
	}

}
