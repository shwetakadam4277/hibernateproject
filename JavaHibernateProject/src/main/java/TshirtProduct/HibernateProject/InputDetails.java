package TshirtProduct.HibernateProject;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputDetails {

	public NecessaryTshirtInput getInputDetails() {
		
		Scanner sc=new Scanner(System.in);
		NecessaryTshirtInput necessary=new NecessaryTshirtInput();
		try {
			System.out.print("Enter Colour of uT-shirt  : ");
			String color = sc.nextLine().toUpperCase();
			necessary.setColor(color);
			System.out.print("Enter Size(S, M, L, XL, XXL)  : ");
			String size = sc.nextLine().toUpperCase();
			necessary.setSize(size);
			System.out.print("Enter Gender for t-shirt(M,F,U)  : ");
			String gender = sc.nextLine().toUpperCase();
			necessary.setGender(gender);
			System.out.print(
					"Enter Output Preference :   1. Price \t 2. Rating  \nEnter Preference Choice Code : ");
			int outputPreference = sc.nextInt();
			necessary.setPreference(outputPreference);
			sc.close();
			
		}catch(InputMismatchException e) {
			System.out.println(e);
		}
		
		return necessary;
	}

}
