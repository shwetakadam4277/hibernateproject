package TshirtProduct.HibernateProject.OutputPreference;

import java.util.Comparator;

import TshirtProduct.HibernateProject.TShirt;

public class ComparePrice implements Comparator<TShirt> {
public int compare(TShirt tshirt1,TShirt tshirt2) {
	return (int)(tshirt1.getPrice()-tshirt2.getPrice());
}
	

}
