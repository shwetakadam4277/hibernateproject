package TshirtProduct.HibernateProject;

import java.util.ArrayList;
import org.hibernate.Session;

public class OutputDetails {


	public void display(ArrayList<TShirt> foundTshirt) {
//		display the data
		System.out.println("***** PRODUCT INFORMATION *****");
		System.out.println("Id|Name|Color|Gender| Size|Price|Rating|Availability");
		for (TShirt tshirt : foundTshirt) {
			System.out.print("| " + tshirt.getId());
			System.out.print(" |" +tshirt.getName());
			System.out.print( "| " +tshirt.getColor());
			System.out.print( "| " +tshirt.getGender());
			System.out.print("| " + tshirt.getSize());
			System.out.print("| " + tshirt.getPrice());
			System.out.print(" |" + tshirt.getRating());
			System.out.print(" |" + tshirt.getAvailability());
			System.out.println("\n");
		}
		if (foundTshirt.isEmpty()) {
			
			System.out.println("T-shirts  Are Not Available.");
		}
		
	}

}
