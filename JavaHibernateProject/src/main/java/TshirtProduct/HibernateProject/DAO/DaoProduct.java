package TshirtProduct.HibernateProject.DAO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.hibernate.query.Query;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.SharedSessionContract;

import TshirtProduct.HibernateProject.TShirt;

public class DaoProduct {
	public List<TShirt> getAllTShirts() throws IOException {
		final SessionFactory factory = Hibernate.getSessionFactory();

		String hql = "From TShirt";
		Session session = factory.openSession();
//		**************************
		String csvFilePath = "C:\\Users\\shwetakadam\\eclipse-workspace\\HibernateProject\\src\\main\\resources\\Assigment Links\\Adidas.csv";
//		BufferedReader lineReader;
//		try {
//			lineReader = new BufferedReader(new FileReader(csvFilePath));
//
//			CSVParser records = CSVParser.parse(lineReader,
//					CSVFormat.EXCEL);
//
//			 session.beginTransaction();
//			for (CSVRecord record : records) {
//				TShirt tshirt = new TShirt();
//				tshirt.setId(record.get(0));
//				tshirt.setName(record.get(1));
//				tshirt.setColor(record.get(2));
//				tshirt.setGender(record.get(3));
//				tshirt.setSize(record.get(4));
//				tshirt.setPrice(Float.parseFloat(record.get(5)));
//				tshirt.setRating(Float.parseFloat(record.get(6)));
//				tshirt.setAvailability(record.get(7));
//				 session.save(tshirt);
//
//			}
//			((SharedSessionContract) session).getTransaction().commit();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}catch(IOException e) {
//			e.printStackTrace();
//		}
//		***********************
		Query query = session.createQuery(hql);
		List<TShirt> foundTshirt = query.list();
		return foundTshirt;
	}
}
