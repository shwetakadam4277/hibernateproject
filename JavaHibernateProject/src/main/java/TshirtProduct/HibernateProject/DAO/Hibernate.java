package TshirtProduct.HibernateProject.DAO;

import java.io.BufferedReader;
import java.io.FileReader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.SharedSessionContract;
import org.hibernate.cfg.Configuration;

import TshirtProduct.HibernateProject.TShirt;

/**
 * Hello world!
 *
 */
public class Hibernate {
	

private static volatile SessionFactory sessionfactory;

	public static SessionFactory getSessionFactory() {

		try {
			if(sessionfactory==null) {
				synchronized(SessionFactory.class) {
					if(sessionfactory==null) {
//						Configuration cfg=new Configuration();
//						cfg.configure("hibernate.cfg.xml");
//						sessionfactory=cfg.buildSessionFactory();
						sessionfactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(TShirt.class).buildSessionFactory();
					}
				}
			}
			

		} catch (Exception e) {
			System.out.println(e);
		}
		return sessionfactory;

	}
}
