package TshirtProduct.HibernateProject;

public class NecessaryTshirtInput {

	private String color;
	private String gender;
	private String size;
	private int preference;
	
	

	public String getColor() {
		return color;
	}



	public void setColor(String color) {
		this.color = color;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public String getSize() {
		return size;
	}



	public void setSize(String size) {
		this.size = size;
	}



	public int getPreference() {
		return preference;
	}



	public void setPreference(int preference) {
		this.preference=preference;
		
	}
	
}
